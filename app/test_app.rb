module TestApp
  class App < Goliath::API
    use Goliath::Rack::Params
    use Goliath::Rack::Render

    def response(env)
      TestApp::API.call(env)
    end
  end
end
