module TestApp
  class API < Grape::API
    prefix 'api'
    format :json
    mount ::TestApp::APIv1
  end
end
