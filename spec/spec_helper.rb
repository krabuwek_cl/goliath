require 'rubygems'

ENV['RACK_ENV'] ||= 'test'

require 'rack/test'
require 'database_cleaner/active_record'
require 'goliath/test_helper'
require_relative 'api_helpers'

require File.expand_path('../config/environment', __dir__)

RSpec.configure do |config|
  config.mock_with :rspec
  config.expect_with :rspec
  config.raise_errors_for_deprecations!
  config.include FactoryBot::Syntax::Methods
  config.include Goliath::TestHelper
  config.include ApiHelpers

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end

  config.before(:suite) do
    FactoryBot.find_definitions
  end
end
