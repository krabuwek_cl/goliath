require 'spec_helper'

describe TestApp::V1::Marks do
  describe 'POST api/v1/marks' do
    around(:example) do |example|
      run_with_api(example)
    end

    let(:user1) { create(:user, login: 'user_name_1') }
    let(:article1) { create(:article, user: user1, ip: '1234') }

    let(:params) { { article_id: article1.id, value: value } }

    before do
      article1
    end

    context 'when mark value is between 0 and 5' do
      let(:value) { 2 }

      it 'creates new mark for article' do
        post_request(path: '/api/v1/marks', body: params) do |async|
          response = JSON.parse(async.response)
          expect(response).to eq({ 'value' => '2.0' })
        end
      end
    end

    context 'when mark value is invalid' do
      let(:value) { 99 }

      it 'returns validation errors' do
        post_request(path: '/api/v1/marks', body: params) do |async|
          response = JSON.parse(async.response)
          expect(response).to eq({ 'value' => ['is not included in the list'] })
        end
      end
    end
  end
end
