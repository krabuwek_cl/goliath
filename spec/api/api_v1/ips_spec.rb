require 'spec_helper'

describe TestApp::V1::Ips do
  describe 'GET api/v1/ips' do
    around(:example) do |example|
      run_with_api(example)
    end

    let(:user1) { create(:user, login: 'user_name_1') }
    let(:user2) { create(:user, login: 'user_name_2') }

    let(:article1) { create(:article, user: user1, ip: '1234') }
    let(:article2) { create(:article, user: user1, ip: '3333') }

    let(:article3) { create(:article, user: user2, ip: '3333') }

    before do
      article1
      article2
      article3
    end

    let(:result) do
      [{ 'ip' => '1234', 'logins' => ['user_name_1'] }, { 'ip' => '3333', 'logins' => %w[user_name_1 user_name_2] }]
    end

    it "returns IP's with loggin" do
      get_request(path: '/api/v1/ips') do |async|
        response = JSON.parse(async.response)
        expect(response).to eq(result)
      end
    end
  end
end
