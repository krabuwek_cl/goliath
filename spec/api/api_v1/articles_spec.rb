require 'spec_helper'

describe TestApp::V1::Articles do
  around(:example) do |example|
    run_with_api(example)
  end

  describe 'POST api/v1/articles' do
    context 'when params is valid' do
      let(:params) do
        {
          content: 'body',
          title: 'title',
          ip: 'ip2',
          login: '1424'
        }
      end

      context 'when user exist' do
        let(:user) { create(:user, login: '1424') }

        before do
          user
        end

        it 'creates article' do
          expect do
            post_request(path: '/api/v1/articles', body: params) do |async|
              response = JSON.parse(async.response)

              expect(response.fetch('content')).to eq('body')
              expect(response.fetch('ip')).to eq('ip2')
              expect(response.fetch('title')).to eq('title')
            end
          end.not_to change { User.count }
        end
      end

      context 'when user not exist' do
        it 'creates article and user' do
          expect do
            post_request(path: '/api/v1/articles', body: params) do |async|
              response = JSON.parse(async.response)

              expect(response.fetch('content')).to eq('body')
              expect(response.fetch('ip')).to eq('ip2')
              expect(response.fetch('title')).to eq('title')
            end
          end.to change { User.count }
        end
      end
    end

    context 'when params is invalid' do
      let(:params) do
        {
          title: 'title',
          ip: 'ip2',
          login: '1424'
        }
      end

      it 'retuns validation errors' do
        post_request(path: '/api/v1/articles', body: params) do |async|
          response = JSON.parse(async.response)
          expect(response).to eq({ 'content' => ["can't be blank"] })
        end
      end
    end
  end

  describe 'GET api/v1/articles/top' do
    let(:article1) { create(:article, marks_sum: 5, marks_count: 1) }
    let(:article2) { create(:article, marks_sum: 4, marks_count: 1) }
    let(:article3) { create(:article, marks_sum: 3, marks_count: 2) }
    let(:article4) { create(:article, marks_sum: 0, marks_count: 0) }

    before do
      article1
      article2
      article3
      article4
    end

    it 'returns top articles' do
      get_request(path: '/api/v1/articles/top') do |async|
        response = JSON.parse(async.response)

        first_article = response.first
        last_article = response.last

        expect(first_article.fetch('marks_avg')).to eq('5.0')
        expect(last_article.fetch('marks_avg')).to eq('0.0')
      end
    end
  end
end
