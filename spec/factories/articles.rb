# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    association :user, factory: :user

    sequence :title do |n|
      "title_#{n}"
    end
    sequence :content do |n|
      "content_#{n}"
    end
    sequence :ip do |n|
      "content_#{n}"
    end
  end
end
