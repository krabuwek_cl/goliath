module ApiHelpers
  def run_with_api(example)
    with_api(TestApp::App) do
      example.run
      EM.stop
    end
  end
end
