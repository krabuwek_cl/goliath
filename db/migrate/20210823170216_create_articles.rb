class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table 'articles', force: :cascade do |t|
      t.references 'user'
      t.string 'ip'
      t.string 'title'
      t.text 'content'
      t.integer 'marks_count', default: 0, null: false
      t.integer 'marks_sum', default: 0, null: false
    end
  end
end
