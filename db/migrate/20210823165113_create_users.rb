class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table 'users', force: :cascade do |t|
      t.string :login, index: { unique: true }
    end
  end
end
