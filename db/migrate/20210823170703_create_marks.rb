class CreateMarks < ActiveRecord::Migration[5.2]
  def change
    create_table 'marks', force: :cascade do |t|
      t.references 'user'
      t.references 'article'
      t.integer 'value', default: 0, null: false
    end
  end
end
