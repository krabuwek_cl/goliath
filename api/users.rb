module TestApp
  class Users < Grape::API
    format :json
    get '/users' do
      [
        {
          user: {
            id: 1
          }
        }
      ]
    end
  end
end
