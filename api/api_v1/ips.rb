module TestApp
  module V1
    class Ips < Grape::API
      format :json

      resource :ips do
        desc "Get IP's with login"
        get do
          ips_with_combined_logins = ArticlesQuery.ips_with_logins

          ips_with_combined_logins.map do |ip_with_combained_logins|
            { ip: ip_with_combained_logins.ip, logins: ip_with_combained_logins.logins }
          end
        end
      end
    end
  end
end
