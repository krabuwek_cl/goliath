module TestApp
  module V1
    class Marks < Grape::API
      format :json

      resource :marks do
        desc 'Create mark'
        post do
          article = ActiveRecord::Base.transaction do
            mark = Mark.new(article_id: params.fetch(:article_id), value: params.fetch(:value))

            error!(mark.errors.messages, 400) if mark.invalid?

            mark.save
            ArticleRatingUpdater.new(mark, params.fetch(:article_id)).call
          end

          { value: article.marks_sum / article.marks_count.to_d }
        end
      end
    end
  end
end
