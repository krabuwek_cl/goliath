module TestApp
  module V1
    class Articles < Grape::API
      format :json

      resource :articles do
        desc 'Create article'
        post do
          status 200
          article = ArticleBuilder.new(params).call

          error!(article.errors.messages, 400) if article.invalid?

          article.save
          article
        end

        desc 'Get Top Articles'
        get :top do
          articles = ArticlesQuery.top_articles(params[:limit])

          articles.map do |article|
            article.slice(:title, :content, :ip, :marks_avg)
          end
        end
      end
    end
  end
end
