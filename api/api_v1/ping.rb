module TestApp
  module V1
    class Ping < Grape::API
      format :json

      get '/ping' do
        { ping: 'pong' }
      end
    end
  end
end
