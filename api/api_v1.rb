module TestApp
  class APIv1 < Grape::API
    format :json
    version :v1

    mount ::TestApp::V1::Ping
    mount ::TestApp::V1::Articles
    mount ::TestApp::V1::Marks
    mount ::TestApp::V1::Ips
  end
end
