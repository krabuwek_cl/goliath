class Mark < ActiveRecord::Base
  belongs_to :user
  belongs_to :article, required: true

  validates :value, inclusion: 0..5
end
