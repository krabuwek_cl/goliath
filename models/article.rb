class Article < ActiveRecord::Base
  belongs_to :user, required: true
  has_many :marks

  validates :title, :content, :ip, presence: true
end
