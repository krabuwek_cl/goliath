$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'api'))
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'app'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'boot'
require 'pry'

Bundler.require :default, ENV['RACK_ENV']

PATHS = [
  '../models/*.rb',
  '../api/api_v1/*.rb',
  '../api/*.rb',
  '../services/*.rb',
  '../services/marks*.rb'
].freeze

def load_path(path)
  Dir[File.expand_path(path, __dir__)].sort.each do |f|
    require f
  end
end

PATHS.each do |path|
  load_path(path)
end

OTR::ActiveRecord.configure_from_file! './config/database.yml'
OTR::ActiveRecord.establish_connection!

require 'goliath'
require 'api'
require 'test_app'
