class ArticleBuilder
  def initialize(params)
    @params = params
  end

  def call
    user = User.find_or_create_by!(login: params[:login])
    Article.new(user: user, title: params[:title], content: params[:content], ip: params[:ip])
  end

  private

  attr_reader :params
end
