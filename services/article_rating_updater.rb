class ArticleRatingUpdater
  def initialize(mark, article_id)
    @mark = mark
    @article_id = article_id
  end

  def call
    article = Article.find(article_id)
    article.increment(:marks_count)
    article.increment(:marks_sum, mark.value)

    article.save
    article
  end

  private

  attr_reader :mark, :article_id
end
