class ArticlesQuery
  def self.top_articles(limit)
    Article.select('*, (CASE WHEN marks_count = 0 THEN 0 ELSE marks_sum::decimal / marks_count END) as marks_avg')
           .order('marks_avg DESC').limit(limit)
  end

  def self.ips_with_logins
    Article.joins(:user).group(:ip).select('articles.ip, array_agg(DISTINCT(users.login)) as logins')
  end
end
