desc 'Generate seed'
task :seed, [:host] do |t, arg|
  require_relative '../../app'
  require_relative '../request_helpers'
  include RequestHelpers

  DEFAULT_HOST = 'http://0.0.0.0:9000'.freeze

  ARTICLES_COUNT = 200_000
  USERS_COUNT = 100
  IPS = (1..50).to_a

  host = arg[:host] || DEFAULT_HOST

  USERS_COUNT.times do |user_number|
    (ARTICLES_COUNT / USERS_COUNT).times do |article_number|
      body = {
        login: "user_login_#{user_number}",
        title: "article_title_#{article_number}",
        content: "article_body_#{article_number}",
        ip: IPS[Random.rand(50)]
      }

      response = make_request(url: "#{host}/api/v1/articles", body: body).body

      article_id = JSON.parse(response).fetch('id')
      while Random.rand(3) > 1
        mark_request_body = { article_id: article_id, value: Random.rand(6) }
        make_request(url: "#{host}/api/v1/marks", body: mark_request_body)
      end
    end
  end
end
