require 'net/http'
module RequestHelpers
  def make_request(url:, body: {})
    Net::HTTP.post URI(url),
                   body.to_json,
                   'Content-Type' => 'application/json'
  end
end
