Grape API on Goliath
====================

Run
---

```
$ bundle install
$ bundle exec ruby app.rb -sv
[92117:INFO] 2013-10-18 12:54:33 :: Starting server on 0.0.0.0:9000 in test mode. Watch out for stones.
```

### Hello World

Navigate to http://0.0.0.0:9000/api/v1/ping with a browser or use `curl`.

```
$ curl http://0.0.0.0:9000/api/ping

{"ping":"pong"}
```

List Routes
-----------

```
bundle exec rake routes
```

Setup Database
-----------
```
bundle exec rake db:setup
```

List Routes
-----------
Run seed
```
bundle exec rake seed
```

